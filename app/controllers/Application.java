package controllers;

import com.google.publicalerts.cap.Alert;
import feed.SimpleFatCapAtomFeedBuilder;
import java.util.*;
import play.mvc.*;

public class Application extends Controller {

    public static void index() {
        List<models.Alert> alerts = models.Alert.find("order by fecha desc").fetch(10);

        List<Alert> rAlerts = new ArrayList<Alert>();
        for (models.Alert alert : alerts) {
            rAlerts.add(alert.getCapAlert());
        }
        
        SimpleFatCapAtomFeedBuilder feedBuilder = new SimpleFatCapAtomFeedBuilder();
        String string = feedBuilder.toAtomFeed("feed", "http://yahoo.com", new Date(), rAlerts);
        
        renderXml(string);
    }

}
