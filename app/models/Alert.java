package models;

import com.google.publicalerts.cap.Area;
import com.google.publicalerts.cap.CapUtil;
import com.google.publicalerts.cap.CapValidator;
import com.google.publicalerts.cap.Circle;
import com.google.publicalerts.cap.Info;
import com.google.publicalerts.cap.Point;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jpa.Model;

/**
 *
 * @author rodolfo
 */
@Entity
public class Alert extends Model {

    @Required
    public String alertId = UUID.randomUUID().toString();
    @Required
    public String sender = "blimp.mx";
    @Required
    public String evento;
    @Required
    @Temporal(TemporalType.TIMESTAMP)
    @As(lang = {"*"}, value = {"yyyy-MM-dd HH:mm:ss"})
    public Date fecha = new Date();
    @Required
    @Enumerated
    public Info.Category categoria;
    @Required
    @Enumerated
    public Info.Urgency urgencia;
    @Required
    @Enumerated
    public Info.Severity severidad;
    @Required
    @Enumerated
    public Info.Certainty certeza;
    @As(lang = {"*"}, value = {"yyyy-MM-dd HH:mm:ss"})
    public Date expira = new Date();
    @Required
    public String headline;
    @Required
    @Column(columnDefinition = "TEXT")
    public String description = "Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C";
    @Required
    @Column(columnDefinition = "TEXT")
    public String instrucciones = "Mash tun bittering hops alcohol ale brewing, cask priming amber shelf life. hop back bitter, hefe lauter tun racking, filter keg alpha acid.";
    @Required
    public String web = "http://blimp.mx";
    @Required
    @Column(columnDefinition = "TEXT")
    public String areaDescription = "The area description";
    @Required
    public Double latitud = 19.4284700;
    @Required
    public Double longitud = -99.1276600;
    @Required
    public Double radio = 200d;

    @Override
    public String toString() {
        return "evento " + evento + " " + fecha;
    }

    public com.google.publicalerts.cap.Alert getCapAlert() {
        com.google.publicalerts.cap.Alert.Builder builder = com.google.publicalerts.cap.Alert.newBuilder().setXmlns(CapValidator.CAP_LATEST_XMLNS);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);

        builder.setIdentifier(alertId)
                .setSender(sender)
                .setSent(CapUtil.formatCapDate(calendar))
                .setStatus(com.google.publicalerts.cap.Alert.Status.ACTUAL)
                .setMsgType(com.google.publicalerts.cap.Alert.MsgType.ALERT)
                .setScope(com.google.publicalerts.cap.Alert.Scope.PUBLIC);

        calendar.setTime(expira);
        // info
        Info.Builder infoBuilder = Info.newBuilder()
                .addCategory(categoria)
                .setEvent(evento)
                .setUrgency(urgencia)
                .setSeverity(severidad)
                .setCertainty(certeza)
                .setHeadline(headline)
                .setDescription(description)
                .setExpires(CapUtil.formatCapDate(calendar))
                .setWeb(web)
                .setInstruction(instrucciones);

        // area
        Area.Builder areaBuilder = Area.newBuilder()
                .setAreaDesc(areaDescription);

        Circle.Builder circleBuilder = Circle.newBuilder();
        Point.Builder pointBuilder = Point.newBuilder();
        pointBuilder.setLatitude(latitud);
        pointBuilder.setLongitude(longitud);
        circleBuilder.setPoint(pointBuilder);
        circleBuilder.setRadius(radio);

        areaBuilder.addCircle(circleBuilder);
        infoBuilder.addArea(areaBuilder);

        builder.addInfo(infoBuilder);
        return builder.build();
    }
}
