![Blimp.mx](http://blimp.mx/firmas/blimp.png)

# Generador de feeds CAP #

Este proyecto proporciona una interfaz gráfica sencilla para generar un [feed CAP](http://docs.oasis-open.org/emergency/cap/v1.2/CAP-v1.2-os.html).

La base de datos reside en memoria, por lo tanto, cada que se inicia el proceso del servidor se genera una base de datos nueva.

## Instalación ##

### Requerimientos ###
* Java 6
* Play 1.2.7 [(¿Cómo Instalar?)](https://www.playframework.com/documentation/1.2.7/install)

### Procedimiento de instalación ###
Clona este repositorio

`git clone https://bitbucket.org/blimpmx/capgenerator.git`

 Hacer que play descargue las dependencias

`play deps capgenerator`

Ejecuta play

`play run capgenerator`

Abre un navegador con la siguiente dirección http://localhost:11000. Se mostrará un feed CAP vacío.

### Agregando alertas ###

Para agregar una nueva alerta abre la siguiente dirección en tu navegador http://localhost:11000/admin/alerts/new

Se mostrará una forma con los campos generales de una alerta CAP.

![capgenerator.png](https://bitbucket.org/repo/GAa7bA/images/2007551440-capgenerator.png)

Una vez que se guardan los datos, la nueva alerta se verá en  http://localhost:11000

### Bibliotecas ###
* [CAP Library](https://code.google.com/p/cap-library/)

## Contacto ##

* Rodolfo Cartas rodolfo@blimp.mx

## Licencia ##
Copyright 2014 CARTAS DIAZ FLORES GURRIA LEAL MARQUEZ y ASOCIADOS SC

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.